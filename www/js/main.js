var main =
{
    myId: 0,
    feedId: 0,
    feedSize: 0,
    username: null,
    feedIsLoading: false,

    /**
     * Start application with calling it
     */
    run: function()
    {
        core.init()

        // load settings
        core.apiGet('/api/settings', function(json)
        {
            if (json.data)
            {
                // you are logged in, that's good
                core.settings = core.extend(core.settings, json.data)
            }

            // we have settings => load language file
            core.loadI18n(function()
            {
                if (main.username)
                {
                    // load feed of 'username'
                    core.apiGet('/api/feed?username=' + main.username, function(json)
                    {
                        if (json.posts)
                        {
                            main.feedId = json.id
                            main.appendPosts(json.posts)
                        }
                        else
                        {
                            core.showModal(core.translate('CANNOT_LOAD_FEED'))
                        }
                    })
                }
                else
                {
                    if (main.myId)
                    {
                        core.apiGet('/api/my-feed', function(json)
                        {
                            if (json.posts)
                            {
                                main.feedId = main.myId
                                main.appendPosts(json.posts)
                            }
                            else
                            {
                                core.showModal(core.translate('CANNOT_LOAD_FEED'))
                            }
                        })
                    }
                    else
                    {
                        // TODO
                        // populate Feed with static file describing the project
                        document.getElementById('feed').innerHTML = document.getElementById('widget-login').innerHTML
                    }
                }
            })

            // not needed anymore
            var initial_loader = document.getElementById('initial-loader')
            initial_loader.parentNode.removeChild(initial_loader)
        })

        // get user from URL
        var p = document.location.href.split('/')

        if (p.length > 3)
        {
            this.username = p[3]
        }

        // bind controls
        // TODO
    },

    /**
     * Add posts to the bottom of the feed
     *
     * @param posts
     */
    appendPosts: function(posts)
    {
        var html = '', l = posts.length, i, postHtml,
            template = document.querySelectorAll('#template-storage .post').innerHTML

        for (i = 0; i < l; i++)
        {
            postHtml = template
            postHtml = core.replaceAll('$ID', posts[i].id, postHtml)
            postHtml = core.replaceAll('$USER_ID', posts[i].user_id, postHtml)
            postHtml = core.replaceAll('$TS', core.formatTs(posts[i].ts), postHtml)
            postHtml = core.replaceAll('$BODY', core.formatPost(posts[i].body), postHtml)
        }

        main.feedSize += l

        // so that generally ajax loading is disabled
        window.onscroll = null

        if (l == 25)
        {
            window.onscroll = function()
            {
                if (main.feedIsLoading)
                {
                    return false
                }

                main.feedIsLoading = true

                if (window.scrollY - 50 >= document.documentElement.scrollHeight - document.body.clientHeight)
                {
                    // TODO: show loader
                    core.apiGet('/api/feed?id=' + main.feedId + '&offset=' + main.feedSize, function(json)
                    {
                        if (json.posts)
                        {
                            main.feedId = json.id
                            main.appendPosts(json.posts)
                        }
                        else
                        {
                            core.showModal(core.translate('CANNOT_LOAD_FEED'))
                        }

                        // TODO: hide loader
                        main.feedIsLoading = false
                    })
                    return false
                }
                return false
            }
        }

        var d1 = document.getElementById('feed')
        d1.insertAdjacentHTML('beforeend', html)
    },

    /**
     * Format a raw HTML post
     *
     * @param html
     * @returns {string}
     */
    formatPost: function(html)
    {
        return ''
    },

    /**
     * Perform a login
     */
    login: function()
    {
        var username = document.getElementById('login-user').value.trim(),
            password = document.getElementById('login-pass').value.trim()



        if (!username)
        {
            document.getElementById('login-user').focus()
            return
        }
        if (!password)
        {
            document.getElementById('login-pass').focus()
            return
        }

        core.apiPost('/api/login', username + '\n' + password, function(json)
        {
            if (json.msg != MSG_SUCCESS)
            {
                core.showModal(core.translate('LOGIN_FAILED'))
            }
        })
    },

    /**
     * Perform a logout
     */
    logout: function()
    {
        core.apiGet('/api/logout')
    }
}