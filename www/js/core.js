const MSG_SUCCESS       = 0
const MSG_NO_SESSION    = 1
const MSG_DEAD_SESSION  = 2
const MSG_LOGIN_FAILED  = 3
const MSG_NO_ROUTE      = 4
const MSG_NO_SUCH_USER  = 5

var core =
{
    modalCallback: null,
    i18n: [],

    settings:
    {
        user_id: 0,
        language: 'en_US'
    },

    /**
     * Inits system listeners
     */
    init: function()
    {
        document.querySelectorAll('#modal button').onclick = function()
        {
            if (core.modalCallback)
            {
                core.modalCallback()
            }
            document.getElementById('modal').style.display = 'none'
        }
    },

    /**
     * GET call to API
     *
     * @param url
     * @param callback
     */
    apiGet: function(url, callback)
    {
        var request = new XMLHttpRequest()
        request.open('GET', url, true)
        request.onload = function()
        {
            if (callback)
            {
                if (this.status >= 200 && this.status < 400)
                {
                    callback(JSON.parse(this.response))
                    return
                }
                else if (this.status >= 502)
                {
                    alert('API is down');
                }

                callback(false)
            }
        }
        request.send()
    },

    /**
     * POST call to API
     *
     * @param url
     * @param data
     * @param callback
     */
    apiPost: function(url, data, callback)
    {
        var request = new XMLHttpRequest()
        request.open('POST', url, true)
        request.onload = function()
        {
            if (callback)
            {
                if (this.status >= 200 && this.status < 400)
                {
                    callback(JSON.parse(this.response))
                }
                else callback(false)
            }
        }
        request.send(data)
    },

    /**
     * Load translations. Support untranslated entries.
     *
     * @param callback
     */
    loadI18n: function(callback)
    {
        var language = this.getSetting('language'),
            request = new XMLHttpRequest

        request.open('GET', '/i18n/en_US.json', true)
        request.onload = function()
        {
            if (this.status >= 200 && this.status < 400)
            {
                core.i18n = JSON.parse(this.response)

                if (language == 'en_US')
                {
                    if (callback) callback(true)
                    return
                }

                request = new XMLHttpRequest
                request.open('GET', '/i18n/' + language + '.json', true)
                request.onload = function()
                {
                    if (this.status >= 200 && this.status < 400)
                    {
                        core.i18n = core.extend(core.i18n, JSON.parse(this.response))
                    }

                    if (callback) callback(true)
                }
            }
            else
            {
                if (callback)
                {
                    console.log('loadI18n() failed')
                    callback(false)
                }
            }
        }
    },

    /**
     * $.extend() analogue
     *
     * @param out
     * @returns {{}}
     */
    extend: function(out)
    {
        out = out || {}

        for (var i = 1; i < arguments.length; i++)
        {
            if (!arguments[i])
                continue

            for (var key in arguments[i])
            {
                if (arguments[i].hasOwnProperty(key))
                    out[key] = arguments[i][key]
            }
        }

        return out
    },

    /**
     * Get a particular setting
     *
     * @param key
     * @returns {null}
     */
    getSetting: function(key)
    {
        return (this.settings[key] === undefined) ? null : this.settings[key]
    },

    /**
     * Show a modal message box
     *
     * @param html
     * @param callback
     */
    showModal: function(html, callback)
    {
        this.modalCallback = callback
        var modal = document.getElementById('modal')
        modal.style.display = ''
        modal.querySelectorAll('.body').innerHTML = html
    },

    /**
     * Return a translation string
     *
     * @param key
     * @returns {string}
     */
    translate: function(key)
    {
        return (this.i18n[key] === undefined) ? '' : this.i18n[key]
    },

    /**
     * PHP str_replace analogue
     *
     * @param find
     * @param replace
     * @param str
     * @returns {string}
     */
    replaceAll: function(find, replace, str)
    {
        return str.replace(new RegExp(find, 'g'), replace)
    },

    /**
     * Prettify UNIX timestamp
     *
     * @param unixTs
     * @returns {string}
     */
    formatTs: function(unixTs)
    {
        return ''
    },

    /**
     * Make a new post
     *
     * @param text
     */
    post: function(text)
    {
        core.apiPost('/api/post', text, function(json)
        {
            if (json.msg != MSG_SUCCESS)
            {
                core.showModal(core.translate('POST_FAILED'))
            }
        })
    }
}
