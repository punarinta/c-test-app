#pragma GCC diagnostic ignored "-Wwrite-strings"

#include "setting.h"

SettingService::SettingService(MySQL *sql)
{
	this->sql = sql;
}

std::vector<Setting> SettingService::findByUserId(uint64_t user_id)
{
	MYSQL_ROW row;
    MYSQL_RES *res;
    Setting setting;
    std::vector<Setting> settings;

    if (!(res = sql->query("SELECT * FROM setting WHERE user_id=" + std::to_string(user_id))))
    {
    	return settings;
    }

    while ((row = mysql_fetch_row(res)) != NULL)
    {
        setting.setUserId(atoi(row[0]));
        setting.setKey(row[1]);
        setting.setValue(row[2]);
    	settings.push_back(setting);
    }

    mysql_free_result(res);
    return settings;
}
