#pragma GCC diagnostic ignored "-Wwrite-strings"

#include "tag.h"

TagService::TagService(MySQL *sql)
{
	this->sql = sql;
}

Tag * TagService::findByValue(std::string value)
{
	MYSQL_ROW row;
    MYSQL_RES *res;

    value = sql->sanitize(value);

    if (!(res = sql->query("SELECT * FROM tag WHERE value=" + value + " LIMIT 1")))
    {
    	return NULL;
    }

    if (!(row = mysql_fetch_row(res)))
    {
        mysql_free_result(res);
    	return NULL;
    }

    Tag *tag = new Tag;

    tag->setId(atoi(row[0]));
    tag->setValue(row[1]);

    mysql_free_result(res);
    return tag;
}

bool TagService::mentionByValue(std::string value, uint64_t post_id)
{
    MYSQL_RES *res;
    uint64_t tag_id;

    // normalize tag value
    value = sql->sanitize(this->normalizeValue(value));

    if (!(res = sql->query("SELECT id FROM tag WHERE value=" + value + " LIMIT 1")))
    {
      	return false;
    }

    if (mysql_num_rows(res))
    {
        MYSQL_ROW user_row = mysql_fetch_row(res);
        sql->query("INSERT INTO post_tag (post_id,tag_id) VALUES (" + std::to_string(post_id) + "," + user_row[0] + ")");
    }
    else
    {
        // create a tag
        sql->query("INSERT INTO tag (value) VALUES (" + value + ")");

        // get its ID
        tag_id = sql->getLastId();
        sql->query("INSERT INTO post_tag (post_id,tag_id) VALUES (" + std::to_string(post_id) + "," + std::to_string(tag_id) + ")");
    }

    mysql_free_result(res);
    return true;
}

std::string TagService::normalizeValue(std::string value)
{
    std::transform(value.begin(), value.end(), value.begin(), ::tolower);
    return value;
}
