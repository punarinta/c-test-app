#ifndef _POST_SERVICE_H_
#define _POST_SERVICE_H_ 1

#include <cstdint>
#include <string>
#include <vector>
#include <string.h>
#include "../core/mysql.h"
#include "../entity/post.h"

class PostService
{
private:
	MySQL *sql;
	std::string queryToJson(std::string);
	void parseBody(std::string, uint64_t);

public:
	PostService(MySQL *);
	Post *findById(uint64_t);
	std::vector<Post> findByUserId(uint64_t, uint64_t offset = 0, uint64_t limit = 25);
	std::string jsonByUserId(uint64_t, uint64_t, uint64_t);
	std::string jsonByTag(std::string, uint64_t, uint64_t);
	uint64_t add(uint64_t, std::string);
	bool remove(uint64_t, uint64_t);
};

#endif
