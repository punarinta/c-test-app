#pragma GCC diagnostic ignored "-Wwrite-strings"

#include "user.h"

UserService::UserService(MySQL *sql)
{
	this->sql = sql;
}

User * UserService::findById(uint64_t id)
{
	MYSQL_ROW row;
    MYSQL_RES *res;

    if (!(res = sql->query("SELECT * FROM user WHERE id=" + std::to_string(id) + " LIMIT 1")))
    {
    	return NULL;
    }

    if (!(row = mysql_fetch_row(res)))
    {
        mysql_free_result(res);
    	return NULL;
    }

    User *user = new User;

    user->setId(atoi(row[0]));
    user->setUsername(row[1]);
    user->setPassword(row[2]);
    user->setEmail(row[3]);
    user->setDisplayName(row[4]);
    user->setActivationToken(row[5]);

    mysql_free_result(res);
    return user;
}

User * UserService::findByUsername(std::string username)
{
	MYSQL_ROW row;
    MYSQL_RES *res;

    username = sql->sanitize(username);

    if (!(res = sql->query("SELECT * FROM user WHERE username='" + username + "' LIMIT 1")))
    {
    	return NULL;
    }

    if (!(row = mysql_fetch_row(res)))
    {
        mysql_free_result(res);
    	return NULL;
    }

    User *user = new User;

    user->setId(atoi(row[0]));
    user->setUsername(row[1]);
    user->setPassword(row[2]);
    user->setEmail(row[3]);
    user->setDisplayName(row[4]);
    user->setActivationToken(row[5]);

    mysql_free_result(res);
    return user;
}

User * UserService::login(std::string username, std::string password)
{
	MYSQL_ROW row;
    MYSQL_RES *res;

    username = sql->sanitize(username);

    if (!(res = sql->query("SELECT * FROM user WHERE username='" + username + "' LIMIT 1")))
    {
    	return NULL;
    }

    if (!(row = mysql_fetch_row(res)))
    {
        mysql_free_result(res);
    	return NULL;
    }

    // generate hash from the provided pass
    char hash[BCRYPT_HASHSIZE];
    bcrypt_hashpw(password.c_str(), (char *) row[2], hash);

	// compare hashes
    if (std::string(hash) != std::string(row[2]))
    {
    	// wrong password
        mysql_free_result(res);
    	return NULL;
    }

    User *user = new User;

    user->setId(atoi(row[0]));
    user->setUsername(row[1]);
    user->setPassword(row[2]);
    user->setEmail(row[3]);
    user->setDisplayName(row[4]);
    user->setActivationToken(row[5]);

    mysql_free_result(res);
    return user;
}

bool UserService::mentionByUsername(std::string username, uint64_t post_id)
{
    MYSQL_RES *res;

    // normalize username
    username = sql->sanitize(this->normalizeUsername(username));

    if (!(res = sql->query("SELECT id FROM user WHERE username=" + username + " LIMIT 1")))
    {
      	return false;
    }

    // if user does not exist just return
    if (!mysql_num_rows(res))
    {
        mysql_free_result(res);
        return false;
    }

    MYSQL_ROW user_row = mysql_fetch_row(res);

    // insert a mention-link
    sql->query("INSERT INTO mention (user_id,post_id) VALUES (" + std::string(user_row[0]) + "," + std::to_string(post_id) + ")");

    mysql_free_result(res);
    return true;
}

std::string UserService::normalizeUsername(std::string username)
{
    std::transform(username.begin(), username.end(), username.begin(), ::tolower);
    return username;
}
