#ifndef _SETTING_SERVICE_H_
#define _SETTING_SERVICE_H_ 1

#include <cstdint>
#include <string>
#include <vector>
#include <string.h>
#include "../core/mysql.h"
#include "../entity/setting.h"

class SettingService
{
private:
	MySQL *sql;

public:
	SettingService(MySQL *);
	std::vector<Setting> findByUserId(uint64_t);
};

#endif
