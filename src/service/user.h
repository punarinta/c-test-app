#ifndef _USER_SERVICE_H_
#define _USER_SERVICE_H_ 1

#include <cstdint>
#include <string>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include "../core/mysql.h"
#include "../core/bcrypt.h"
#include "../entity/user.h"

class UserService
{
private:
	MySQL *sql;
    std::string normalizeUsername(std::string);

public:
	UserService(MySQL *);
	User *login(std::string, std::string);
	User *findById(uint64_t);
	User *findByUsername(std::string);
	bool mentionByUsername(std::string, uint64_t);
};

#endif
