#pragma GCC diagnostic ignored "-Wwrite-strings"

#include "post.h"
#include "tag.h"
#include "user.h"
#include "../core/util.h"

PostService::PostService(MySQL *sql)
{
	this->sql = sql;
}

Post * PostService::findById(uint64_t id)
{
	MYSQL_ROW row;
    MYSQL_RES *res;

    if (!(res = sql->query("SELECT * FROM post WHERE id=" + std::to_string(id) + " LIMIT 1")))
    {
    	return NULL;
    }

    if (!(row = mysql_fetch_row(res)))
    {
        mysql_free_result(res);
    	return NULL;
    }

    Post *post = new Post;

    post->setId(atoi(row[0]));
    post->setUserId(atoi(row[1]));
    post->setTs(atoi(row[2]));
    post->setBody(row[3]);

    mysql_free_result(res);
    return post;
}

std::vector<Post> PostService::findByUserId(uint64_t user_id, uint64_t offset, uint64_t limit)
{
    MYSQL_RES *res;
    std::vector<Post> posts;

    if (!(res = sql->query("SELECT * FROM post WHERE user_id=" + std::to_string(user_id) + " ORDER BY ts DESC LIMIT " + std::to_string(offset) + "," + std::to_string(limit))))
    {
    	return posts;
    }

	MYSQL_ROW row;
    Post post;

    while ((row = mysql_fetch_row(res)) != NULL)
    {
        post.setId(atoi(row[0]));
        post.setUserId(atoi(row[1]));
        post.setTs(atoi(row[2]));
        post.setBody(row[3]);

        posts.push_back(post);
    }

    mysql_free_result(res);
    return posts;
}

std::string PostService::queryToJson(std::string query)
{
    MYSQL_RES *res;

    if (!(res = sql->query(query)))
    {
    	return "[]";
    }

	MYSQL_ROW row;
    std::string s = "[";
    unsigned int i = 0;

    while ((row = mysql_fetch_row(res)) != NULL)
    {
        if (i)
        {
            s = s + ",";
        }
        s = s + "{\"id\":" + row[0] + ",";
        s = s + "\"user_id\":" + row[1] + ",";
        s = s + "\"ts\":" + row[2] + ",";
        s = s + "\"body\":\"" + str_replace("\n", "\\n", row[3]) + "\"}";

        i++;
    }

    mysql_free_result(res);
    return s + "]";
}

std::string PostService::jsonByUserId(uint64_t user_id, uint64_t offset, uint64_t limit)
{
    return this->queryToJson("SELECT * FROM post WHERE user_id=" + std::to_string(user_id) + " ORDER BY ts DESC LIMIT " + std::to_string(offset) + "," + std::to_string(limit));
}

std::string PostService::jsonByTag(std::string tag, uint64_t offset, uint64_t limit)
{
    return this->queryToJson("SELECT * FROM post AS p JOIN post_tag AS pt ON pt.post_id=p.id JOIN tag AS t ON t.id=pt.tag_id WHERE t.value=" + tag + " ORDER BY p.ts DESC LIMIT " + std::to_string(offset) + "," + std::to_string(limit));
}

uint64_t PostService::add(uint64_t user_id, std::string body)
{
    uint64_t post_id, ts = time(NULL);

    body = sql->sanitize(body);
    sql->query("INSERT INTO post (user_id,ts,body) VALUES (" + std::to_string(user_id) + "," + std::to_string(ts) + ",'" + body + "')" );

    post_id = sql->getLastId();

    // scan for #tags and @user mentions
    this->parseBody(body, post_id);

    return post_id;
}

bool PostService::remove(uint64_t post_id, uint64_t user_id)
{
    MYSQL_RES *res;

    if (!(res = sql->query("DELETE FROM post WHERE id=" + std::to_string(post_id) + " AND user_id=" + std::to_string(user_id) + " LIMIT 1" )))
    {
    	return false;
    }

    mysql_free_result(res);

    return true;
}

void PostService::parseBody(std::string body, uint64_t post_id)
{
    size_t pos = 0, sPos = 0;
    std::string tagname = "", username = "";

    TagService *tagService = new TagService(this->sql);
    UserService *userService = new UserService(this->sql);

    // search for tags
    while (1)
    {
        pos = body.find('#', pos);

        if (pos == -1)
        {
            break;
        }

        if (!pos || body[pos-1] == ' ' || body[pos-1] == '\n')
        {
            // OK, that's a valid hash starter, check the hash body

            sPos = body.find_first_of(" \n\r,.", pos);

            if (sPos != -1)
            {
                tagname = body.substr(pos, sPos-pos);
                pos += sPos;
                tagService->mentionByValue(tagname, post_id);
            }
            else
            {
                tagname = body.substr(pos);
                tagService->mentionByValue(tagname, post_id);
                break;
            }
        }
    }

    pos = 0;

    // search for tags
    while (1)
    {
        pos = body.find('@', pos);

        if (pos == -1)
        {
            break;
        }

        if (!pos || body[pos-1] == ' ' || body[pos-1] == '\n')
        {
            // OK, that's a valid username starter, check the username body

            sPos = body.find_first_of(" \n\r,", pos);

            if (sPos != -1)
            {
                username = body.substr(pos, sPos-pos);
                pos += sPos;
                userService->mentionByUsername(username, post_id);
            }
            else
            {
                username = body.substr(pos);
                userService->mentionByUsername(username, post_id);
                break;
            }
        }
    }

    // cleanup
    delete tagService;
    delete userService;
}

