#ifndef _TAG_SERVICE_H_
#define _TAG_SERVICE_H_ 1

#include <cstdint>
#include <string>
#include "../core/mysql.h"
#include "../entity/tag.h"

class TagService
{
private:
	MySQL *sql;
	std::string normalizeValue(std::string);

public:
	TagService(MySQL *);
	Tag *findByValue(std::string);
	bool mentionByValue(std::string, uint64_t);
};

#endif
