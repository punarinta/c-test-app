#ifndef _SETTING_ENTITY_H_
#define _SETTING_ENTITY_H_ 1

#include <cstdint>
#include <string>

class Setting
{
private:
	uint64_t user_id = 0;
	std::string key = "";
	std::string value = "";

public:
    Setting();
    Setting(std::string, std::string);
	uint64_t getUserId();
	void setUserId(uint64_t);
	std::string getKey();
	void setKey(std::string);
	std::string getValue();
	void setValue(std::string);
};

#endif
