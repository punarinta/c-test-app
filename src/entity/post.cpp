#include "post.h"

uint64_t Post::getId()
{
	return this->id;
}

void Post::setId(uint64_t id)
{
	this->id = id;
}

uint64_t Post::getUserId()
{
	return this->user_id;
}

void Post::setUserId(uint64_t user_id)
{
	this->user_id = user_id;
}

uint64_t Post::getTs()
{
	return this->ts;
}

void Post::setTs(uint64_t ts)
{
	this->ts = ts;
}

std::string Post::getBody()
{
	return this->body;
};

void Post::setBody(std::string body)
{
	this->body = body;
};
