#ifndef _POST_TAG_ENTITY_H_
#define _POST_TAG_ENTITY_H_ 1

#include <cstdint>
#include <string>

class PostTag
{
private:
	uint64_t post_id = 0;
	uint64_t tag_id = 0;

public:
	uint64_t getPostId();
	void setPostId(uint64_t);
	uint64_t getTagId();
	void setTagId(uint64_t);
};

#endif
