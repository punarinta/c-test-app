#include "mention.h"

uint64_t Mention::getUserId()
{
	return this->user_id;
}

void Mention::setUserId(uint64_t user_id)
{
	this->user_id = user_id;
}

uint64_t Mention::getPostId()
{
	return this->post_id;
}

void Mention::setPostId(uint64_t post_id)
{
	this->post_id = post_id;
}
