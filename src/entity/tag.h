#ifndef _TAG_ENTITY_H_
#define _TAG_ENTITY_H_ 1

#include <cstdint>
#include <string>

class Tag
{
private:
	uint64_t id = 0;
	std::string value = "";

public:
	uint64_t getId();
	void setId(uint64_t);
	std::string getValue();
	void setValue(std::string);
};

#endif
