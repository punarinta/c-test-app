#include "tag.h"

uint64_t Tag::getId()
{
	return this->id;
}

void Tag::setId(uint64_t id)
{
	this->id = id;
}

std::string Tag::getValue()
{
	return this->value;
};

void Tag::setValue(std::string value)
{
	this->value = value;
};

