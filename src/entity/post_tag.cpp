#include "post_tag.h"

uint64_t PostTag::getPostId()
{
	return this->post_id;
}

void PostTag::setPostId(uint64_t post_id)
{
	this->post_id = post_id;
}

uint64_t PostTag::getTagId()
{
	return this->tag_id;
}

void PostTag::setTagId(uint64_t tag_id)
{
	this->tag_id = tag_id;
}
