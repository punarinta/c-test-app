#ifndef _POST_ENTITY_H_
#define _POST_ENTITY_H_ 1

#include <cstdint>
#include <string>

class Post
{
private:
	uint64_t id = 0;
	uint64_t user_id = 0;
	uint64_t ts = 0;
	std::string body = "";

public:
	uint64_t getId();
	void setId(uint64_t);
	uint64_t getUserId();
	void setUserId(uint64_t);
	uint64_t getTs();
	void setTs(uint64_t);
	std::string getBody();
	void setBody(std::string);
};

#endif
