#ifndef _USER_ENTITY_H_
#define _USER_ENTITY_H_ 1

#include <cstdint>
#include <string>

class User
{
private:
	uint64_t id = 0;
	std::string username = "";
	std::string display_name = "";
	std::string password = "";
	std::string email = "";
	std::string activation_token = "";

public:
	uint64_t getId();
	void setId(uint64_t);
	std::string getUsername();
	void setUsername(std::string);
	std::string getDisplayName();
	void setDisplayName(std::string);
	std::string getPassword();
	void setPassword(std::string);
	std::string getEmail();
	void setEmail(std::string);
	std::string getActivationToken();
	void setActivationToken(std::string);
};

#endif
