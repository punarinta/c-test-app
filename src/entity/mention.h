#ifndef _MENTION_ENTITY_H_
#define _MENTION_ENTITY_H_ 1

#include <cstdint>
#include <string>

class Mention
{
private:
	uint64_t user_id = 0;
	uint64_t post_id = 0;

public:
	uint64_t getUserId();
	void setUserId(uint64_t);
	uint64_t getPostId();
	void setPostId(uint64_t);
};

#endif
