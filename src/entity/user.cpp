#include "user.h"

uint64_t User::getId()
{
	return this->id;
}

void User::setId(uint64_t id)
{
	this->id = id;
}

std::string User::getUsername()
{
	return this->username;
};

void User::setUsername(std::string s)
{
	this->username = s;
};

std::string User::getDisplayName()
{
	return this->display_name;
};

void User::setDisplayName(std::string s)
{
	this->display_name = s;
};

std::string User::getPassword()
{
	return this->password;
};

void User::setPassword(std::string s)
{
	this->password = s;
};

std::string User::getEmail()
{
	return this->email;
};

void User::setEmail(std::string s)
{
	this->email = s;
};

std::string User::getActivationToken()
{
	return this->activation_token;
};

void User::setActivationToken(std::string s)
{
	this->activation_token = s;
};
