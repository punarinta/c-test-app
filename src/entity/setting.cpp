#include "setting.h"

Setting::Setting()
{
}

Setting::Setting(std::string key, std::string value)
{
    this->setKey(key);
    this->setValue(value);
}

uint64_t Setting::getUserId()
{
	return this->user_id;
}

void Setting::setUserId(uint64_t user_id)
{
	this->user_id = user_id;
}

std::string Setting::getKey()
{
	return this->key;
};

void Setting::setKey(std::string key)
{
	this->key = key;
};

std::string Setting::getValue()
{
	return this->value;
};

void Setting::setValue(std::string value)
{
	this->value = value;
};
