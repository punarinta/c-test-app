#pragma GCC diagnostic ignored "-Wwrite-strings"
#pragma GCC diagnostic ignored "-Wunused-result"

#include "util.h"

std::vector<std::string> &explode(char delim, const std::string &s, std::vector<std::string> &elems)
{
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim))
    {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> explode(char delim, const std::string &s)
{
    std::vector<std::string> elems;
    explode(delim, s, elems);
    return elems;
}

std::string trim(std::string s)
{
    return s.erase(s.find_last_not_of(" \n\r\t") + 1);
}

std::string get_cookie(std::string &cookieString, const char *name)
{
    std::vector<std::string> pair, cookies = explode(';', cookieString);

    for (int i = cookies.size(); i--;)
    {
        pair = explode('=', trim(cookies[i]));

        if (pair[0] == name)
        {
            return pair[1];
        }
    }

    return "";
}

std::string get_param(std::string &paramString, const char *name)
{
    std::vector<std::string> pair, params = explode('&', paramString);

    for (int i = params.size(); i--;)
    {
        pair = explode('=', trim(params[i]));

        if (pair[0] == name)
        {
            return pair[1];
        }
    }

    return "";
}

std::string random_string(size_t length)
{
    auto randchar = []() -> char
    {
        const char charset[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(length, 0);
    std::generate_n(str.begin(), length, randchar);
    return str;
}

void session_save(std::string id, void *ptr, uint32_t size)
{
    id = "sessions/" + id;

    FILE *fp = fopen(id.c_str(), "wb");
    fwrite(&size, 4, 1, fp);
    fwrite(ptr, size, 1, fp);
    fclose(fp);
}

void session_load(std::string id, void *out_ptr, uint32_t *size)
{
    id = "sessions/" + id;
    FILE *fp = fopen(id.c_str(), "rb");
    if (!fp)
    {
        return;
    }

    fread(size, 4, 1, fp);

    // create buffer and fill it
    fread(out_ptr, *size, 1, fp);
    fclose(fp);
}

void session_load(std::string id, void *out_ptr)
{
    uint32_t *size = new uint32_t;
    session_load(id, out_ptr, size);
}

bool session_remove(std::string id)
{
    id = "sessions/" + id;
    if (!remove(id.c_str()))
    {
        return true;
    }
    else
    {
        return false;
    }
}

std::string str_replace(const std::string& from, const std::string& to, std::string str)
{
    size_t start_pos = 0;

    while ((start_pos = str.find(from, start_pos)) != std::string::npos)
    {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }

    return str;
}