#ifndef _UTIL_H_
#define _UTIL_H_ 1

#include <sstream>
#include <string>
#include <vector>
#include <string.h>
#include <algorithm>

std::vector<std::string> &explode(char, const std::string &, std::vector<std::string> &);
std::vector<std::string> explode(char, const std::string &);
std::string get_cookie(std::string &, const char *);
std::string get_param(std::string &, const char *);
std::string trim(std::string);
std::string random_string(size_t);
void session_save(std::string, void *, uint32_t);
void session_load(std::string, void *, uint32_t *);
void session_load(std::string, void *);
bool session_remove(std::string);
std::string str_replace(const std::string&, const std::string&, std::string);

#endif
