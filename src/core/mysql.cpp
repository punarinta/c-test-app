#include "mysql.h"
#include "util.h"

MySQL::MySQL(const char *server, unsigned int port, const char *user, const char *password, const char *database)
{
	this->connection = mysql_init(NULL);
	if (!mysql_real_connect(this->connection, server, user, password, database, port, NULL, 0))
	{
		printf("DB error: %s\n", mysql_error(this->connection));
	}
	else
	{
		// UTF-8 out of the box
		this->query("SET NAMES utf8");
	}
}

MySQL::~MySQL()
{
	mysql_close(this->connection);
}

std::string MySQL::sanitize(std::string query)
{
    return str_replace("'", "\\'", query);
}

MYSQL_RES* MySQL::query(std::string query)
{
	if (mysql_query(this->connection, (char*) query.c_str()))
	{
		printf("DB error: %s\n", mysql_error(this->connection));
		return NULL;
	}
	else return mysql_use_result(this->connection);
}

uint64_t MySQL::getLastId()
{
    return mysql_insert_id(this->connection);
}