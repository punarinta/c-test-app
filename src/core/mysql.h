#ifndef _MYSQL_H_
#define _MYSQL_H_ 1

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <regex>
#include <mysql/mysql.h>

class MySQL
{
private:
	MYSQL *connection;

public:
	MySQL(const char *server, unsigned int port, const char *user, const char *password, const char *database);
	~MySQL();
	MYSQL_RES* query(std::string);
	std::string sanitize(std::string);
	uint64_t getLastId();
};

#endif
