#pragma GCC diagnostic ignored "-Wwrite-strings"

#include "api.h"

void *thread(void *a)
{
    int i, rc; 
    User *me;
    uint64_t myId;
    FCGX_Request request; 

    if (FCGX_InitRequest(&request, socketId, 0) != 0) 
    { 
        printf("Can not init request\n"); 
        return NULL; 
    } 
    
    streambuf *cin_streambuf  = cin.rdbuf();
    streambuf *cout_streambuf = cout.rdbuf();
    streambuf *cerr_streambuf = cerr.rdbuf();

    // setup services
    MySQL *sql = new MySQL("localhost", 0, "root", NULL, "test_db");
    UserService *userService = new UserService(sql);
    PostService *postService = new PostService(sql);
    SettingService *settingService = new SettingService(sql);

    for (;;) 
    {
        static pthread_mutex_t accept_mutex = PTHREAD_MUTEX_INITIALIZER; 

        pthread_mutex_lock(&accept_mutex); 
        rc = FCGX_Accept_r(&request); 
        pthread_mutex_unlock(&accept_mutex); 

        if (rc < 0) 
        { 
            printf("Can not accept new request\n"); 
            break; // continue; (?)
        } 

        fcgi_streambuf cin_fcgi_streambuf(request.in);
        fcgi_streambuf cout_fcgi_streambuf(request.out);
        fcgi_streambuf cerr_fcgi_streambuf(request.err);
        cin.rdbuf(&cin_fcgi_streambuf);
        cout.rdbuf(&cout_fcgi_streambuf);
        cerr.rdbuf(&cerr_fcgi_streambuf);

        // start headers
        cout << "Content-type: application/json\r\n"
             << "Access-Control-Allow-Origin: *\r\n"
             << "Server: nginx\r\n";        

        string uri = FCGX_GetParam("DOCUMENT_URI", request.envp),
               query = FCGX_GetParam("QUERY_STRING", request.envp),
               cookies = FCGX_GetParam("HTTP_COOKIE", request.envp),
               method = FCGX_GetParam("REQUEST_METHOD", request.envp);
        string sessionId = get_cookie(cookies, "__session_id");

        printf("Requested: %s\n", uri.c_str()); 


        // reset ID
        myId = 0;

        // routing
        if (sessionId.empty())
        {
            if (uri == "/api/auth/login")
            {
                // process login
                string content = get_request_content(request);
                vector<string> credentials = explode('\n', content.c_str());

                if (credentials.size() != 2)
                {
                    cout << "\r\n{\"msg\":" << MSG_LOGIN_FAILED <<"}\n";
                    next_request();
                }

                if (!(me = userService->login(credentials[0], credentials[1])))
                {
                    cout << "\r\n{\"msg\":" << MSG_LOGIN_FAILED << "}";
                }
                else
                {
                    sessionId = random_string(32);
                    uint64_t myId = me->getId();
                    session_save(sessionId, &myId, 8);
                    cout << "Set-Cookie: __session_id=" << sessionId << "; path=/api\r\n";
                    cout << "\r\n{\"msg\":" << MSG_SUCCESS <<"}\n";
                }
                next_request();
            }
        }
        else
        {
            session_load(sessionId, &myId);

            if (!myId)
            {
                // cannot read session from file
                cout << "Set-Cookie: __session_id=logout; path=/api; expires=Fri, 17 Jan 1986 07:15:00 GMT\r\n\r\n{\"msg\":" << MSG_DEAD_SESSION << "}";
                next_request();
            }

            me = userService->findById(myId);
        }


        // returns feed of an arbitrary user, no me-related data should be served here
        if (uri == "/api/feed")
        {
            uint64_t offset = 0;
            User *feedUser  = NULL;
            string userId_s = get_param(query, "id"),
                   offset_s = get_param(query, "offset"),
                   username = get_param(query, "username");

            if (!offset_s.empty())
            {
                offset = stoi(offset_s);
            }

            if (!username.empty())
            {
                // if username is provided find User
                feedUser = userService->findByUsername(username);
            }
            else
            {
                if (userId_s.empty() && myId)
                {
                    // no user, it's ME
                    feedUser = me;
                }
                else if (!userId_s.empty())
                {
                    uint64_t userId = stoi(userId_s);
                    feedUser = userService->findById(userId);
                }
            }

            if (!feedUser)
            {
                cout << "\r\n{\"msg\":" << MSG_NO_SUCH_USER << "}";
                next_request();
            }

            cout << "\r\n{\"data\":" << postService->jsonByUserId(feedUser->getId(), offset, 25) << ", \"id\":" << to_string(feedUser->getId()) << "}\n";
            next_request();
        }


        // force check session
        if (sessionId.empty())
        {
            cout << "\r\n{\"msg\":" << MSG_NO_SESSION << "}";
            next_request();
        }

        /***********************************************

                "myId" is never 0 below this line

         ***********************************************/

        // returns my feed
        if (uri == "/api/my-feed")
        {
            uint64_t offset = 0;
            string offset_s = get_param(query, "offset");
            if (!offset_s.empty())
            {
                offset = stoi(offset_s);
            }

            cout << "\r\n{\"data\":" << postService->jsonByUserId(myId, offset, 25) << "}\n";
            next_request();
        }

        // returns all my settings
        if (uri == "/api/settings")
        {
            vector<Setting> settings = settingService->findByUserId(myId);

            cout << "\r\n{\"data\":{\"user_id\":\"" << to_string(myId) << "\"";

            for (unsigned int i = settings.size();i--;)
            {
                cout << ",\"" << settings[i].getKey() << "\":\"" << settings[i].getValue() << "\"";
            }

            cout << "}}";

            next_request();
        }

        // POST here your post
        if (uri == "/api/post")
        {
            int msg = MSG_SUCCESS;
            string content = get_request_content(request);

            if (method == "POST")
            {
                if (content.size() > 146)
                {
                    msg = MSG_POST_TOO_LONG;
                }
                else
                {
                    if (!postService->add(myId, content))
                    {
                        msg = MSG_POST_FAILED;
                    }
                }
            }
            else if (method == "DELETE")
            {
                uint64_t postId = stoi(content);
                postService->remove(postId, myId);
            }
            else
            {
                msg = MSG_NOT_SUPPORTED;
            }

            cout << "\r\n{\"msg\":\"" << to_string(msg) << "\"}\n";
            next_request();
        }

        // call to logout
        if (uri == "/api/auth/logout")
        {
            session_remove(sessionId);
            cout << "Set-Cookie: __session_id=logout; path=/api; expires=Fri, 17 Jan 1986 07:15:00 GMT\r\n\r\n{\"msg\":" << MSG_SUCCESS << "}";
            next_request();
        }

        // default action
        cout << "\r\n{\"msg\":" << MSG_NO_ROUTE <<"}\n";
        next_request();
    }

    cin.rdbuf(cin_streambuf);
    cout.rdbuf(cout_streambuf);
    cerr.rdbuf(cerr_streambuf);

    return NULL;
}