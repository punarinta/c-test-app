#include "api.h"

#define THREAD_COUNT 8
#define SOCKET_PATH ":8000"

string get_request_content(const FCGX_Request & request)
{
    const unsigned long STDIN_MAX = 1000000;
    char * content_length_str = FCGX_GetParam("CONTENT_LENGTH", request.envp);
    unsigned long content_length = STDIN_MAX;

    if (content_length_str)
    {
        content_length = strtol(content_length_str, &content_length_str, 10);
        if (*content_length_str)
        {
            cerr << "Can't Parse 'CONTENT_LENGTH='"
                 << FCGX_GetParam("CONTENT_LENGTH", request.envp)
                 << "'. Consuming stdin up to " << STDIN_MAX << endl;
        }

        if (content_length > STDIN_MAX)
        {
            content_length = STDIN_MAX;
        }
    }
    else
    {
        // Do not read from stdin if CONTENT_LENGTH is missing
        content_length = 0;
    }

    char * content_buffer = new char[content_length];
    cin.read(content_buffer, content_length);
    content_length = cin.gcount();

    // Chew up any remaining stdin - this shouldn't be necessary
    // but is because mod_fastcgi doesn't handle it correctly.

    // ignore() doesn't set the eof bit in some versions of glibc++
    // so use gcount() instead of eof()...
    do cin.ignore(1024); while (cin.gcount() == 1024);

    string content(content_buffer, content_length);
    delete [] content_buffer;
    return content;
}

int main(void) 
{ 
    int i; 
    pthread_t id[THREAD_COUNT]; 

    FCGX_Init(); 
    umask(0);   
    socketId = FCGX_OpenSocket(SOCKET_PATH, 2000); 
    if (socketId < 0) 
    { 
        return 1; 
    }

    printf("Socket is opened\n");

    if (mysql_library_init(0, NULL, NULL))
    {
        fprintf(stderr, "Could not initialize MySQL library\n");
        return 1;
    }

    for (i = 0; i < THREAD_COUNT; i++) 
    { 
        pthread_create(&id[i], NULL, thread, NULL); 
    } 

    for (i = 0; i < THREAD_COUNT; i++) 
    { 
        pthread_join(id[i], NULL); 
    }

    mysql_library_end();

    return 0; 
}