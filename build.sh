#!/bin/sh
g++ \
src/core/*.cpp \
src/entity/*.cpp \
src/service/*.cpp \
api-thread.cpp \
api.cpp \
\
src/vendor/bcrypt/crypt_gensalt.c \
src/vendor/bcrypt/crypt_blowfish.c \
src/vendor/bcrypt/wrapper.c \
\
-lfcgi++ -lfcgi -lmysqlclient_r -std=c++11 \
-O2 -o application -lpthread


# spawn-fcgi -p 8000 -n application
# multithreading -- http://stackoverflow.com/questions/18666835/nginx-fastcgi-multithreading
