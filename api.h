#ifndef _API_H_
#define _API_H_ 1

#define MSG_SUCCESS         0
#define MSG_NO_SESSION      1
#define MSG_DEAD_SESSION    2
#define MSG_LOGIN_FAILED    3
#define MSG_NO_ROUTE        4
#define MSG_NO_SUCH_USER    5
#define MSG_POST_TOO_LONG   6
#define MSG_POST_FAILED     7
#define MSG_NOT_SUPPORTED   8

#include <iostream>
#include <cstdint>
#include <vector>
#include <pthread.h> 
#include <sys/types.h>
#include <sys/stat.h>

#include "fcgio.h"

#include "src/core/mysql.h"
#include "src/core/util.h"
#include "src/core/bcrypt.h"

#include "src/service/user.h"
#include "src/service/setting.h"
#include "src/service/post.h"

#define next_request() FCGX_Finish_r(&request); continue;

using namespace std;

static int socketId;
void *thread(void *);
string get_request_content(const FCGX_Request &);

#endif